#!/usr/bin/python3

from mailtrail import MailTrail
from mistert_base import MrtBase 

"""
Class for printing summary delivery stats.
"""

class MrtStats(MrtBase):
    sender = None
    message_id = None
    host_type = None
    stats = {} 
    duplicate_queue_ids = []
    allowed_duplicate_ids = []

    def main(self):
        mailtrail = MailTrail()
        mailtrail.since = self.since
        mailtrail.until = self.until
        mailtrail.log_level = mailtrail.elastic.log_level = self.log_level
        if not self.sender and not self.host_type:
            # Check if this is going to take a while.
            seconds = self.calculate_range(self.since, self.until)
            if seconds > 60 * 60 * 24:
                self.cprint("[y]Warning[/y]: This will take a while and will probably give you results that are overwhelming. Try adding --sender or --recipient or --host-type, or restrict to a smaller time period.")
        data = mailtrail.main(sender=self.sender, host_type=self.host_type, message_id=self.message_id)
        self.process_data(data)

    def process_data(self, data_set):
        client_map = {}
        for key in data_set.keys():
            data = data_set[key]["summary"]
            sender = data.get('sender')
            from_address = data.get('from')
            deliveries = data.get('deliveries')
            client_ip = data.get('client_ip')
            client_hostname = data.get('client_hostname')
            flags = data.get('flags')

            if sender:
                index = sender
            else:
                index = from_address

            # The MAILER-DAEMON sends messages without a from address 
            # and does very little except bounce, so leave them out.
            if not index:
                print("no index from is {0}".format(from_address))
                continue

            # Here we track the client IP address.
            if sender not in client_map:
                client_map[index] = []

            client = None
            # We want to know if the logins are coming from many different
            # networks. IP addresses are not reliable because someone might
            # connect from three different Google IPs and that should only
            # count as one network. So, if we have a reverse lookup, let's only
            # count the last two parts of the domain name.
            if client_hostname:
                hostname_parts = client_hostname.split(".")
                length = len(hostname_parts)
                # We might get "localhost" or some other invalid response.
                if (length > 1):
                    client = "{0}.{1}".format(hostname_parts[length-2], hostname_parts[length-1])

            if not client:
                # Prefer hostname, but if we don't get a reverse lookup we use the IP address.
                client = client_ip
            if client:
                if not client in client_map[index]:
                    client_map[index].append(client)

            if index not in self.stats.keys():
                self.stats[index] = {
                    "connections": 0,
                    "ips": 0,
                    "total": 0,
                    "bounced": 0,
                    "sent": 0,
                    "expired": 0,
                    "deferred": 0,
                    "virus": 0,
                    "spam": 0
                 }
            total = 0
            bounced = 0
            sent = 0
            deferred = 0
            expired = 0

            if "VIRUS" in flags:
                self.stats[index]["virus"] += 1 
            if "SPAM" in flags:
                self.stats[index]["spam"] += 1 

            for delivery in deliveries:
                total += 1
                if delivery['status'] == "sent":
                    sent += 1
                elif delivery['status'] == "bounced":
                    bounced += 1
                elif delivery['status'] == "deferred":
                    deferred += 1
                elif delivery['expired'] == "expired":
                    expired += 1


            self.stats[index]["connections"] += 1 
            self.stats[index]["ips"] = len(client_map[index]) 
            self.stats[index]["total"] += total
            self.stats[index]["bounced"] += bounced
            self.stats[index]["sent"] += sent 
            self.stats[index]["deferred"] += deferred 
            self.stats[index]["expired"] += expired 
            
            total = self.stats[index]["total"]
            sent = self.stats[index]["sent"]
            # Somehow this can be zero if there are no deliveries associate with
            # this message.
            if total > 0:
                self.stats[index]["failure_rate"] = round(((total - sent) / total) * 100)

    def report(self):
        total_connections = 0
        total_total = 0
        total_sent = 0
        total_bounced = 0
        total_deferred = 0
        total_expired = 0
        total_virus = 0
        total_spam = 0
        total_failure_rate = 0

        for key in self.stats.keys():
            connections = self.stats[key].get("connections")
            ips = self.stats[key].get("ips")
            total = self.stats[key].get("total")
            failure_rate = self.stats[key].get("failure_rate")
            sent = self.stats[key].get("sent")
            bounced = self.stats[key].get("bounced")
            deferred = self.stats[key].get("deferred")
            expired = self.stats[key].get("expired")
            virus = self.stats[key].get('virus')
            spam = self.stats[key].get('spam')
            spam_virus = "{0}/{1}".format(spam, virus)
            
            # Keep totals
            total_connections += connections
            total_total += total 
            total_sent += sent 
            total_bounced += bounced 
            total_deferred += deferred 
            total_expired += expired 
            total_virus += virus 
            total_spam += spam 

            # Adjust formatting.
            if spam > 0 or virus > 0:
                spam_virus = "[r]{0}[/r]".format(spam_virus)
            if bounced > 0:
                bounced = "[y]{0}[/y]".format(bounced)
            if deferred > 0:
                deferred = "[y]{0}[/y]".format(deferred)

            self.cprint("[c]sender:[/c] {0}, [c]connections:[/c] {1}, [c]ips:[/c] {2}, [c]failure rate:[/c] {3}, [c]messages:[/c] {4}, [c]sent:[/c] {5}, [c]bounced:[/c] {6}, [c]deferred:[/c] {7}, [c]expired:[/c] {8}, [c]spam/virus:[/c] {9}".format(key, connections, ips, failure_rate, total, sent, bounced, deferred, expired, spam_virus))

        # Calculate grand totals
        if len(self.stats.keys()) > 1:
            total_spam_virus = "{0}/{1}".format(total_spam, total_virus)
            if total_total > 0:
                total_failure_rate = round((total_total - total_sent) / total_total * 100)

            print()
            self.cprint("[y]TOTAL:[/y] [c]connections:[/c] {0}, [c]failure rate:[/c] {1}, [c]messages:[/c] {2}, [c]sent:[/c] {3}, [c]bounced:[/c] {4}, [c]deferred:[/c] {5}, [c]expired:[/c] {6}, [c]spam/virus:[/c] {7}".format(total_connections, total_failure_rate, total_total, total_sent, total_bounced, total_deferred, total_expired, total_spam_virus))



