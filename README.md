# Mr. T

[Mr. T](https://code.mayfirst.org/mfmt/mistert) provides a number of email
reporting features based on mail log data collected by [elastic
search](https://elastic.co).

## Setup

`mistert` traces messages using the `queue_id`, which on a default postfix
installation, will include duplicate queue id. Therefore, be sure to set
[`enable_long_queue_ids`](http://www.postfix.org/postconf.5.html#enable_long_queue_ids).

In addition, `mistert` depends on a number of elastic search fields being
present that must be configured via the [dissect
beat](https://www.elastic.co/guide/en/beats/functionbeat/current/dissect.html)
feature. Specifically:

 * `dissect.client_hostname`
 * `dissect.client_ip`
 * `dissect.postfix_from_address`
 * `dissect.postfix_instance`
 * `dissect.postfix_orig_queue_id`
 * `dissect.postfix_queued_as_id`
 * `dissect.postfix_queue_id`
 * `dissect.postfix_sasl_username`
 * `dissect.postfix_status`
 * `dissect.postfix_status_message`
 * `dissect.postfix_to_address`
 * `dissect.postfix_message_id`

## Usage

`mistert` takes two arguments and a number of sub-commands.

Arguments:

 * `--since`: specify how far back in the logs to start the search in the
   format `<number><unit>`, i.e., 15m (default), 2h, 3d, or 1w.
 * `--until`: specify how far back in the logs to end the search, using the
   same foramt as `--since`. Default: 0 (now).
 * `--log-level`: specify 'error'

Subcommands:

 * `list`: print one line of output for every message received. You can limit by recipient, sender or message-id. 
 * `stats`: print one line of output for every sender or recipient, including
   the status of the message delivery (same options as `list`)
* `alert`: print one line of output for every sender or recipient if the delivery
   patterns seem suspicious
 * `prebounce`: generate or retrieve a list of email addresses that have bounced
   using pre-configured bounce messages known to indicate a permanent failure with
   the address.
 * `blocks`: print a list of bounce messages that probably indicate that the sending
   server is on a block list.

You can pass `--help` after the subcommand for more options.

## Why Mr T

[Mr T](https://en.wikipedia.org/wiki/Mr._T) is an actor who was discovered by
Sylvester Stallone after winning NBC's "America's Toughest Bouncer"
competition.
