#!/usr/bin/env python3

"""
Elastic is May First's elasticsearch python querying library. At some point we
should move to the standard elasticsearch python libraries. This library was
written to better understand the elastic search query syntax without any
abstractions.
"""

import requests
import json
import os
import time
import yaml
import time
from mistert_base import MrtBase

class Elastic(MrtBase):
    # Elastic credentials
    elastic_host = None
    elastic_user = None
    elastic_password = None
    mistert_conf = [ 'mistert.conf', '/etc/mistert.conf' ]
    # Elastic point in time variable
    pit = None

    def __init__(self):
        self.set_elastic_credentials()

    def set_elastic_credentials(self):
        conf = None
        for conf_path in self.mistert_conf:
            if os.path.exists(conf_path):
                with open(conf_path) as f:
                    conf = yaml.load(f, Loader=yaml.FullLoader)
        if conf is None:
            raise Exception("Failed to find mistert.conf")
        self.elastic_password = conf["elastic_password"] 
        self.elastic_host = conf["elastic_host"] 
        self.elastic_user = conf["elastic_user"] 

    def __del__(self):
        self.del_pit()

    """
    A PIT is a point in time marker. When we include our PIT in our paging queries, it ensures
    that we get consistent results.
    """
    def set_pit(self):
        path = 'journalbeat-*/_pit?keep_alive=1m'
        json_result = self.post(path) 
        if "id" not in json_result:
            raise RunTimeError("Failed to get id from pit request: {0}.".format(r.json()))
        self.pit = json_result['id']

    def del_pit(self):
        # FIXME - mistert user does not have permission to delete.
        return
        if not self.pit:
            return
        path = "/_pit"
        q = {
            "id": self.pit
        }
        self.delete(path, q)

    """
    Given a list, return a new list in which the original list is broken up
    into groups of <limit> items. This helper function helps us ensure we
    never send an elastic terms query with more then the allowed number of
    items. See self.query_all_terms().
    """
    def chunkify_list(self, items, limit=50000):
        chunked_list = list()
        for i in range(0, len(items), limit):
            chunked_list.append(items[i:i+limit])
        return chunked_list


    """
    Post to the elastic search server 
    """
    def post(self, path, q=None):
        return self.send("post", path, q)

    """
    Delete to the elastic search server.
    """
    def delete(self, path, q=None):
        return self.send("delete", path, q)

    """
    Send data to the elastic search server
    """
    def send(self, http_type, path, q=None, attempt=0):
        headers = { 'Content-Type': 'application/json' }
        url = self.elastic_host + path
        data = None
        if q:
            data = json.dumps(q)

        start = time.time()
        self.cprint(".", self.LOG_LEVEL_INFO, end="")
        if http_type == "post":
            r = requests.post(url, headers=headers, data=data, auth=(self.elastic_user, self.elastic_password))
        elif http_type == "delete":
            r = requests.delete(url, headers=headers, data=data, auth=(self.elastic_user, self.elastic_password))
        else:
            raise Excpetion("Unknown http type: {0}".format(http_type))
        end = time.time()
        self.cprint("Query time: {0} seconds".format(end - start), self.LOG_LEVEL_DEBUG)


        if r.status_code != 200:
            try:
                error = r.json()
            except json.decoder.JSONDecodeError:
                # If the elasticsearch server is really borked there is no telling what it will return.
                error = r.text()
            message = "Failed to get a 200 status code from elastic search. Received code: {0} and text: {1}. Query was {2}".format(r.status_code, error, str(q)[0:1000])
            if r.status_code == 404:
                # This seems to happen if an index is rotating.
                if attempt > 2:
                    # We tried three times. Giving up.
                    raise RuntimeError(message)
                # Wait 5 seconds and try again.
                time.sleep(5)
                attempt += 1
                return self.send(http_type, path, q=q, attempt=attempt)
            raise RuntimeError(message)

        return r.json()

    """
    Send a query to the elastic search server.
    """
    def query(self, q, path=None):
        if not path:
            if "pit" in q:
                path = '_search'
            else:
                path = 'journalbeat-*/_search'

        json_result = self.post(path, q)
        if "hits" not in json_result or "total" not in json_result["hits"] or "value" not in json_result["hits"]["total"]:
            raise RuntimeError("Failed to get proper structure of json output: {0}.".format(r.json()))
        return json_result

    """
    Send a query to the json server, but page the results to ensure we get everything.
    Return just the hits.
    """
    def query_all(self, q):
        if not self.pit:
            self.set_pit()

        q['pit'] = {
            'id': self.pit,
            'keep_alive': '5m'
        }
        if "size" not in q:
            # Set the maximum size.
            q["size"] = 10000

        if 'sort' not in q:
            raise RuntimeError("Please include a sort key in a query_all.")

        search_after = None
        all_hits = [] 

        while [ 1 ]:
            active_q = q.copy()
            if search_after:
                active_q["search_after"] = search_after

            result = self.query(active_q)
            hits = result["hits"]["hits"]
            if len(hits) == 0:
                break

            for hit in hits:
                all_hits.append(hit)
                search_after = hit['sort']

        return all_hits

    """
    Just like query_all, but add a terms query to the query with the provided terms, ensuring
    that we don't exceed the 65K terms limit. Return just the hits.
    """
    def query_all_terms(self, q, terms_field, terms):
        chunks = self.chunkify_list(terms)
        all_hits = []
        for chunk in chunks:
            chunk_q = q.copy()
            chunk_q["query"]["bool"]["filter"].append({ 
                "terms": {
                    terms_field: chunk 
                }
            })
            hits = self.query_all(chunk_q)
            if len(hits) == 0:
                break

            for hit in hits:
                all_hits.append(hit)
        return all_hits



