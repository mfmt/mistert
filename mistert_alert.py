#!/usr/bin/python3

from mistert_stats import MrtStats 
import sys 
from mistert_base import MrtBase

"""
Class for printing alert for questionable senders.
"""

class MrtAlert(MrtBase):
    location_limit = 5
    delivery_failure_limit = 20
    host_type = None
    stats = {} 

    def main(self):
        stats = MrtStats()
        stats.log_level = self.log_level
        stats.since = self.since
        stats.until = self.until
        stats.host_type = self.host_type
        stats.main() 
        self.stats = stats.stats
        self.report()

    def report(self):
        exit_code = 0
        for key in self.stats.keys():
            connections = self.stats[key].get("connections")
            ips = self.stats[key].get("ips")
            total = self.stats[key].get("total")
            failure_rate = self.stats[key].get("failure_rate")
            sent = self.stats[key].get("sent")
            bounced = self.stats[key].get("bounced")
            deferred = self.stats[key].get("deferred")
            expired = self.stats[key].get("expired")
            virus = self.stats[key].get('virus')
            spam = self.stats[key].get('spam')
            spam_virus = "{0}/{1}".format(spam, virus)

            if total < 50:
                # Hard to assess a sender if they have sent less then 50 messages.
                continue

            if key == "-":
                # The MAILER DAEMON comes through without a from address (to avoid loops)
                # and always has a lot of bounces, so ignore.
                continue

            alert = False
            if ips > self.location_limit or failure_rate > self.delivery_failure_limit:
                alert = True

            if alert:
                exit_code = 1
                self.cprint("sender: {0}, connections: {1}, ips: {2}, failure rate: {3}, messages: {4}, sent: {5}, bounced: {6}, deferred: {7}, expired: {8}, spam/virus: {9}".format(key, connections, ips, failure_rate, total, sent, bounced, deferred, expired, spam_virus), self.LOG_LEVEL_ERROR)

        if exit_code != 0:
            sys.exit(exit_code)


