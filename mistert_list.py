#!/usr/bin/python3

from elastic import Elastic
from mailtrail import MailTrail 
import re
from mistert_base import MrtBase

"""
Class for listing messages sent via our network.
"""

class MrtList(MrtBase):
    since = None
    until = None
    sender = None
    recipient = None
    message_id = None
    status = None
    host_type = None
    list_recipients = False

    def main(self):
        mailtrail = MailTrail()
        mailtrail.since = self.since
        mailtrail.until = self.until
        mailtrail.log_level = mailtrail.elastic.log_level = self.log_level
        if not self.sender and not self.recipient and not self.host_type and not self.message_id:
            seconds = self.calculate_range(self.since, self.until)
            if seconds > 60 * 60 * 24:
                self.cprint("[y]Warning[/y]: This will take a while and will probably give you results that are overwhelming. Try adding --sender or --recipient or --host-type, or restrict to a smaller time period.")

        data = mailtrail.main(sender=self.sender, recipient=self.recipient, host_type=self.host_type, message_id=self.message_id)
        self.report(data)

    def tell_user(self, msg):
        if self.verbose:
            print(msg)

    def report(self, data_set):
        for key in data_set:
            values = data_set[key]['summary']
            timestamp = values.get("timestamp")
            from_address = values.get("from")
            deliveries = values.get("deliveries")
            sender = values.get("sender")
            queue_ids = values.get('queue_ids')
            flags = values.get('flags')
            message_id = values.get('message_id')
            hostname = values.get('hostname', "")

            # When passing both a sender and a recipient, mailtrail does a
            # OR query, including both. So, we have to filter out all messages
            # except the ones matching both recipient and sender here.
            if self.sender and self.recipient:
                # Filter by sender.
                if self.sender != from_address and self.sender != sender:
                    continue

                # Filter by recipient
                found = False
                for delivery in deliveries:
                    if delivery.get('to_address') == self.recipient:
                        found = True
                        break
                if found == False:
                    continue

            timestamp = timestamp[5:-5]
            # Filter based on user arguments.
            if self.status:
                # If no deliveries match the user requested status, then move on.
                res = list(filter(lambda deliveries: deliveries['status'] == self.status, deliveries))
                if not res:
                    continue
            
            if from_address == "-" and len(deliveries) == 0:
                # The message may have just been queued and we don't have all the data yet.
                continue

            if sender:
                from_address = "{0} ({1})".format(sender, from_address)

            self.cprint("[c]{}[/c] From: [g]{: <8}[/g] Via: [c]{: <8}[/c] [y]{}[/y] Id: {}".format(timestamp, from_address,  hostname, ",".join(flags), message_id ))
            if self.list_recipients:
                for delivery in deliveries:
                    status_message = delivery.get('status_message') 
                    to_address = delivery.get('to_address')
                    delivered_to_address = delivery.get('delivered_to_address')
                    if delivered_to_address:
                        delivered_to_address = ",".join(delivered_to_address)
                    host = delivery.get('host')
                    status = delivery.get('status')
                    displayed_status_message = ""
                    if status != "sent":
                        status = "[r]{0}[/r]".format(status)
                        displayed_status_message = status_message
                    timestamp = delivery.get("timestamp")
                    # Shorten the timestamp field. Shave off year from the front and milliseconds from the end.
                    timestamp = timestamp[5:-5]

                    if delivered_to_address:
                        to_address = "{0} ({1})".format(to_address, delivered_to_address)
                    self.cprint("    [c]{}[/c] {: <45} {} to: [y]{}[/y] {}".format(timestamp, host, status, to_address, displayed_status_message))
