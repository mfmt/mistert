#!/usr/bin/env python3

"""
When a message travels through our network it goes through several servers
making it hard to track.

mailtrail is a class for tracking these messages by starting with a unique
message id and then finding every queue id that is related to it.

mailtrail returns a dictionary keyed to the message id that provides detailed
and compreshensive information about how that message travelled through the
network.

Typically usage:
    mailtail = MailTrail()
    mailtrail.since = '5m'
    mailtril.until = '0s'
    # Optionally set filters. 
    data = mailtrail.main(recpient=recipient, sender=sender, host_type=host_type)
"""

from elastic import Elastic
import re
import string
import random
import time
from mistert_base import MrtBase

class MailTrail(MrtBase):
    # The elastic object.
    elastic = None

    """
    If a sender or recipient is specified, build a list of queue ids belonging
    to the sender or recipient so we can restrict the message_ids to a smaller
    set. By default it's set to None to indicate we should not try to filter by
    queue ids.
    """
    queue_ids_filter = None 

    """
    Optionally filter the initial set of message ids by host type. 
    """
    host_type_filter = None

    """
    queue_id_map

    One email message will have multiple queue ids as it travels through the
    network (a new queue id created every time it passes to a new server or
    service). For each single message, there will be multiple keys representing
    each queue_id, but all will point to the same value (the message id). This
    way you can lookup any queue id you find and know which message it belongs
    to.
    """ 
    queue_id_map = {}

    """
    messages

    A dict containing the populated information about the messages.

    It has the structure:

      "message_id" =>  (the key, representing a unique message)
        "logs" => list (a list of all raw log lines we can link to this message
          id)
        "summary" => A structured dict summarizing what we know about this message.

    """
    messages= {}

    """
    Detect a verp style from address. 

    In short: one or more characters that is anything except a + or an *,
    followed by a single + or *, followed by one or more characters that is not
    an @ sign, followed by an @ sign, followed by anything else. Most VERP
    messages use "+" but listserv uses an *.
    """
    verp_re = re.compile('([^+*]+)[+*][^@]+(@.*)')
    
    def __init__(self):
        self.elastic = Elastic()

    """
    Optionally, before calling main() you can set filters to limit the results.
    """
    def set_filters(self, recipient=None, sender=None, host_type=None, message_id=None): 

        if message_id and (sender or recipient):
            raise Exception("You cannot set both message_id and also sender and recipient.")

        if recipient or sender or message_id:
            # Convert the self.queue_ids_filter to a list so we can
            # append to it and we know it should be populated.
            self.queue_ids_filter = []
            self.set_queue_ids_filter(recipient=recipient, sender=sender, message_id=message_id)
        if host_type:
            if host_type == 'cf':
                self.host_type_filter = "mailcf*"
            elif host_type == 'mx':
                self.host_type_filter = "mailmx*"
            elif host_type == 'relay':
                self.host_type_filter = "mailrelay*"
            elif host_type == 'store':
                self.host_type_filter = "mailstore*"
            else:
                raise Exception("Unknown host type {0}".format(host_type))

    """
    If a recipient or sender is specified, create a list of queue_ids that
    should be used to filter the results when building our list to report.
    """
    def set_queue_ids_filter(self, sender=None, recipient=None, message_id=None):
        if not sender and not recipient and not message_id:
            raise Excpetion("Please pass either sender or recipient or message_id to set_queue_ids_filter.")

        q = {
            "sort": [
                { "dissect.postfix_queue_id" : {"order": "asc"}}
            ],
            "_source": [
                    "dissect.postfix_queue_id", 
            ],
            "query": {
                "bool": {
                    "filter": [
                        { "range": { "@timestamp": { "gte": "now-{0}".format(self.since), "lte": "now-{0}".format(self.until) }}},
                        { "term": { "systemd.slice": "system-postfix.slice" }},
                        { "exists": { "field": "dissect.postfix_queue_id" } },
                    ]
               }
            }
        }

        if message_id:
                q["query"]["bool"]["filter"].append(
                    { "term": { "dissect.postfix_message_id": message_id } }
                )
        elif recipient or sender:
            # Note: this builds an OR query - matching both sender or recipient. It's up
            # to the calling class to filter the results by both recipient and sender.
            q["query"]["bool"]["should"] = []
            q["query"]["bool"]["minimum_should_match"] = 1 
            if recipient:
                if '*' in recipient:
                    q["query"]["bool"]["should"].append(
                        { "wildcard": { "dissect.postfix_to_address": { "value": recipient } }}
                    )
                else:
                    q["query"]["bool"]["should"].append(
                        { "term": { "dissect.postfix_to_address": recipient } }
                    )

            if sender:
                # Note: the presence of an @ sign means we need to search both the from address
                # but also the still the sasl_username, because sometimes these get recorded
                # with the @ address attached (e.g. username@mail.mayfirst.org).
                if '@' in sender:
                    if '*' in sender:
                        q["query"]["bool"]["should"].append(
                                { "wildcard": { "dissect.postfix_from_address": { "value": sender } }}
                        )
                        q["query"]["bool"]["should"].append(
                                { "wildcard": { "dissect.postfix_sasl_username": { "value": sender } }}
                        )
                    else:
                        q["query"]["bool"]["should"].append(
                            { "term": { "dissect.postfix_from_address": sender } }
                        )
                        q["query"]["bool"]["should"].append(
                            { "term": { "dissect.postfix_sasl_username": sender } }
                        )
                else:
                    if '*' in sender:
                        q["query"]["bool"]["should"].append(
                                { "wildcard": { "dissect.postfix_sasl_username": { "value": sender } }}
                        )
                    else:
                        q["query"]["bool"]["should"].append(
                            { "term": { "dissect.postfix_sasl_username": sender } }
                        )

        # Gather queue ids.
        self.cprint("Initial filter query", self.LOG_LEVEL_DEBUG)
        hits = self.elastic.query_all(q)
        self.cprint("Initial filter query hits: {0}".format(len(hits)), self.LOG_LEVEL_DEBUG)
        for hit in hits:
            queue_id = hit["_source"]["dissect"]["postfix_queue_id"]
            if queue_id not in self.queue_ids_filter:
                self.queue_ids_filter.append(queue_id)


    def main(self, sender=None, recipient=None, host_type=None, message_id=None):
        if sender or recipient or host_type or message_id:
            # Set filters that will limit the number of message ids we gather
            # in the next step.
            self.cprint("Setting initial filters.", self.LOG_LEVEL_INFO, end="")
            self.set_filters(sender=sender, recipient=recipient, host_type=host_type, message_id=message_id)

        # Populate the self.messages dict with an initial set of message
        # ids that we will report on. It's automatically filtered by since
        # and until and optinally filtered by recipient, sender or host type
        # if set above.
        self.cprint("", self.LOG_LEVEL_INFO)
        self.cprint("Setting initial queue ids.", self.LOG_LEVEL_INFO, end="")
        self.set_initial_queue_ids()

        # Gather every log line available for any message related to our initial
        # queue id.
        self.cprint("", self.LOG_LEVEL_INFO)
        self.cprint("Populating the logs", self.LOG_LEVEL_INFO, end="")
        self.populate_logs()

        # Spam logs are keyed to message id so have to be added separately.
        self.cprint("", self.LOG_LEVEL_INFO)
        self.cprint("Adding spam logs.", self.LOG_LEVEL_INFO, end="")
        self.add_spam_logs()

        # Parse the logs to produce a summary for each message 
        self.cprint("", self.LOG_LEVEL_INFO)
        self.cprint("Summarizing messages.", self.LOG_LEVEL_INFO)
        self.cprint("", self.LOG_LEVEL_INFO)
        self.summarize_messages()

        return self.messages

    """
    Perform a search for initial message ids. This method determines which
    messages on which we will return data.
    """
    def set_initial_queue_ids(self):
        # We use the message_id field as a way to find unique messages in our network.
        q = {
            "sort": [
                { 
                    "dissect.postfix_message_id" : {"order": "asc"},
                }
            ],
            "_source": [
                    "@timestamp",
                    "dissect.postfix_queue_id", 
                    "dissect.postfix_message_id", 
            ],
            "query": {
                "bool": {
                    "filter": [
                        { "range": { "@timestamp": { "gte": "now-{0}".format(self.since), "lte": "now-{0}".format(self.until) }}},
                        { "term": { "systemd.slice": "system-postfix.slice" }},
                        { "exists": { "field": "dissect.postfix_message_id" } },
                        { "exists": { "field": "dissect.postfix_queue_id" } }
                    ],
                    "must_not": [
                        # The mailfilter servers should never originate an email, so we will only get
                        # duplicates.
                        { "wildcard": { "host.hostname": { "value": "mailfilter*" }}}
                    ]
               }
            }
        }

        self.cprint("Setting initial message ids", self.LOG_LEVEL_DEBUG)
        if self.host_type_filter:
            q["query"]["bool"]["filter"].append(
                { "wildcard": { "host.hostname": { "value": self.host_type_filter } }}
            )
        if self.queue_ids_filter is None:
            # Nothing else to filter. 
            hits = self.elastic.query_all(q)
        else:
            if len(self.queue_ids_filter) == 0:
                # Return an empty dict - if you filter by sender or recipient and there are
                # no matching entries for the sender or recipient, we stop early and return
                # nothing.
                return
            # Run the same query but restrict to the given queue ids.
            hits = self.elastic.query_all_terms(q, terms=self.queue_ids_filter, terms_field="dissect.postfix_queue_id")
        report = {}
        self.cprint("Pre-filtered Initial message id count: {0}".format(len(hits)), self.LOG_LEVEL_DEBUG)
        generated_message_id_count = 0
        for hit in hits:
            queue_id = hit["_source"]["dissect"]["postfix_queue_id"]
            message_id = hit["_source"]["dissect"]["postfix_message_id"]
            timestamp = hit["_source"]["@timestamp"]
            if not message_id:
                # So many broken email clients. Sigh. We will generate our own
                # message id. It's quite likely that a message will be
                # submitted to the MX server without a message id so we will
                # generate one here. Then, we'll encounter the same message
                # being submitted to the mailrelay server (still without a
                # message id). As a result we will have two messages that are
                # part of the same chain, with two different, generated message
                # ids. That's ok. We will detect those in the trace_queue_ids
                # function and merge them as we see them.
                random_user_portion = ''.join(random.choice(string.ascii_letters + string.digits) for x in range(20))
                message_id = "{0}@mayfirst.generated.message.id".format(random_user_portion)
                generated_message_id_count += 1
            elif queue_id in self.queue_id_map.keys():
                entered_message_id = self.queue_id_map[queue_id]
                if message_id != entered_message_id:
                    raise Excpetion("Duplicate queue id found: {0}, message ids: {1}, {2}".format(queue_id, message_id, entered_message_id  ))
                continue

            # Map this queue id back to the message id.
            self.queue_id_map[queue_id] = message_id

            if message_id not in self.messages.keys():
                # And initialize the messages dict with initial summary data.
                self.messages[message_id] = {
                    'logs': [],
                    'summary': {
                        "timestamp": timestamp, 
                        # deliveries is a list of all the delivery lines for each recipient of the message.
                        "deliveries": [],
                        "from": "-",
                        # The sender is either the sasl username responsible for the message or a truncated
                        # version of the from address if it's a long verp-style from.
                        "sender": "",
                        "client_ip": "",
                        "client_hostname": "",
                        "flags": [],
                        "queue_ids": [ queue_id ]
                    }
                }
            else:
                if queue_id not in self.messages[message_id]["summary"]["queue_ids"]:
                    self.messages[message_id]["summary"]["queue_ids"].append(queue_id)

        self.cprint("We generated {0} message ids".format(generated_message_id_count), self.LOG_LEVEL_DEBUG)


    """
    Provided a dict of message id => queue id pairs, populate the logs key for
    each message in self.messages.
    """
    def populate_logs(self):
        self.cprint("Setting raw logs", self.LOG_LEVEL_DEBUG)
        
        # Make a list of all the queue_ids.
        search_queue_ids = list(self.queue_id_map.keys())
        self.cprint(" ({0} initial queue ids) ".format(len(search_queue_ids)), self.LOG_LEVEL_INFO, end="")
        # Every search produces related queue ids, then we have to search for
        # those related queue ids until there are no more queue ids to find.
        while (len(search_queue_ids) > 0):
            # The next set of queue ids to search starts off empty.
            next_queue_ids = []
            # Map all records matching the list of queue ids against the postfix_queue_id field...
            next_queue_ids += self.trace_queue_ids(search_queue_ids, "dissect.postfix_queue_id") 
            # ... the postfix_orig_queue_id.
            next_queue_ids += self.trace_queue_ids(search_queue_ids, "dissect.postfix_orig_queue_id") 
            # ... and the postfix_queued_as_id.
            next_queue_ids += self.trace_queue_ids(search_queue_ids, "dissect.postfix_queued_as_id") 

            # Now search again using the queue ids we found.
            search_queue_ids = next_queue_ids



    """
    Given a list of queue ids and a field to search, look for elastic search
    entries that indicate a hand off from one queue id to another. Populate
    both the self.queue_id_map (so we can always link one queue id to the right
    message_id) and the self.messages[message_id]["logs"] dict with the newly
    found logs for the tracked message_id. Return newly found queue_ids so they
    can be searched next.
    """
    def trace_queue_ids(self, search_queue_ids, terms_field):
        q =  {
            "sort": [
                {"@timestamp": {"order": "asc"}}
            ],
            "_source": [
                "@timestamp",
                "host.hostname",
                "dissect.*"
            ],
            "query": {
                "bool": {
                    "filter": [
                        { "range": { "@timestamp": { "gte": "now-{0}".format(self.since), "lte": "now-{0}".format(self.until)}}},
                        { "term": { "systemd.slice": "system-postfix.slice" }},
                    ],
               },
            },
        }
        hits = self.elastic.query_all_terms(q, terms_field=terms_field, terms=search_queue_ids)
        # Keep track of the queue ids we found so we can return them to be searched next.
        next_queue_ids = []
        for hit in hits:
            queued_as_id = orig_queue_id = None
            dissect = hit["_source"]["dissect"]
            queue_id = dissect["postfix_queue_id"]
            hostname = hit["_source"]["host"]["hostname"]
            timestamp = hit["_source"]["@timestamp"]

            if "postfix_queued_as_id" in dissect:
                queued_as_id = dissect["postfix_queued_as_id"]
            if "postfix_orig_queue_id" in dissect:
                orig_queue_id = dissect["postfix_orig_queue_id"]

            # We need to populate both the queue_id_map and the log_lines_map.

            # Figure out which field we searched on for this result.
            search_field = terms_field.replace("dissect.", "")
            # Get the value of the search field that matched.
            search_value = dissect[search_field]

            # Now let's figure out the result value that we found with this
            # search value.
            result_value = None
            if search_field == "postfix_queue_id":
                # If we searched on postfix_queue_id then the result we
                # found might either be queued_as_id if we relayed this
                # message to another server or orig_queue_id if we passed
                # this on internally to the spam/clam service.
                if queued_as_id:
                    result_value = queued_as_id
                elif orig_queue_id:
                    result_value = orig_queue_id
            else:
                # Otherwise, the result value is always the queue id.
                result_value = queue_id

            # This result ultimately maps to the following message id.
            # Assuming we are within range.
            message_id = self.queue_id_map[search_value]
            
            # Add a few fields to our dict so we can more easily append it
            # to the logs dict.
            dissect['timestamp'] = timestamp
            dissect['hostname'] = hostname

            # If we found a new queue id, then add it to the map and append
            # to the next_queue_ids list.
            if result_value:
                if result_value not in self.queue_id_map.keys():
                    self.queue_id_map[result_value] = message_id
                    next_queue_ids.append(result_value)
                
            # Always append to the logs. 
            self.messages[message_id]["logs"].append(dissect)
        return next_queue_ids 

    """
    Spam ID happens against the message id, not the queue id, so we don't get
    it unless we make a separate query.
    """
    def add_spam_logs(self):
        q =  {
            "sort": [
                { "dissect.postfix_message_id" : { "order": "asc"}}
 
            ],
            "_source": [
                "@timestamp",
                "host.hostname",
                "dissect.*"
            ],
            "query": {
                "bool": {
                    "filter": [
                        { "range": { "@timestamp": { "gte": "now-{0}".format(self.since), "lte": "now-{0}".format(self.until)}}},
                        { "term": { "dissect.spampd_status": "spam" }}
                    ]
               }
            }
        }
        hits = self.elastic.query_all_terms(q, terms_field='dissect.postfix_message_id', terms=list(self.messages.keys()))
        for hit in hits:
            dissect = hit["_source"]["dissect"]
            message_id = dissect["postfix_message_id"]
            dissect['timestamp'] = hit["_source"]['@timestamp']
            dissect['hostname'] = hit["_source"]['host']['hostname']
            self.messages[message_id]['logs'].append(dissect)

    """
    Populate the 'summary' key of the self.messages property with a compact summary of
    the log lines.
    """
    def summarize_messages(self):
        self.cprint("Initial message count: {0}".format(len(self.messages)), self.LOG_LEVEL_DEBUG)
        start = time.time()
        for message_id in self.messages.keys():
            # Sort by timestamp. This isn't 100% reliable since logs are not
            # always recorded in the exact order that the events happen, but
            # since it's mostly accurate we do make some assumptions that log
            # entries that came later happened later.
            sorted_lines = sorted(self.messages[message_id]["logs"], key=lambda d: d['timestamp']) 
            #print()
            #print(message_id)

            # Every message has one and only one sender, but ultimately can
            # have multiple recipients (to, cc, bcc and also an alias that
            # expands to multiple recipients). So, for every message, we keep a
            # delivery_dict keyed to each recipient we find - that way we can
            # track the relay host, delivery status and delivery time for each
            # recipient separately.
            delivery_dict = {}

            # Go through every log line we found and try to create a sensibly
            # structured dict that provides some meaning.
            for line in sorted_lines:
                hostname = line.get('hostname') 
                timestamp = line.get('timestamp')
                from_address = line.get('postfix_from_address')
                to_address = line.get('postfix_to_address')
                orig_to_address = line.get('postfix_orig_to_address')
                relay = line.get('postfix_relay')
                status = line.get('postfix_status')
                status_message = line.get('postfix_status_message')
                spampd_status = line.get('spampd_status')
                queue_id = line.get('postfix_queue_id')
                sasl_username = line.get('postfix_sasl_username')
                client_hostname = line.get('client_hostname')
                client_ip = line.get('client_ip')

                # Keep track of whether this log line should populate the
                # delivery dictionary.
                # The to_address is the original to address
                delivery_to_address = None
                # The delivered to address is for addresses with aliases 
                delivery_delivered_to_address = None
                delivery_host = None
                delivery_status = None
                delivery_timestamp = None
                delivery_status_message = None

                # Always record the message id.
                self.messages[message_id]["summary"]["message_id"] = message_id

                # Add flags for viruses or spam.
                if status_message:
                    delivery_status_message = status_message
                    if status_message == '250 Virus Detected; Discarded Email':
                        self.messages[message_id]["summary"]["flags"].append('VIRUS')
                if spampd_status and spampd_status == 'spam':
                    self.messages[message_id]["summary"]["flags"].append('SPAM')

                # If this message was sent via an authenticated sender, track the IP
                # of the sender (useful for detecting compromised accounts)
                if sasl_username:
                    self.messages[message_id]["summary"]['sender'] = sasl_username
                    self.messages[message_id]["summary"]['client_ip'] = client_ip
                    self.messages[message_id]["summary"]['client_hostname'] = client_hostname

                # Let's also track client IPs for mx and mailrelay servers (the client
                # IPs we *don't* care about our filter and origin servers). Just
                # be sure we don't overwrite a client_ip set by the cf server.
                if hostname.startswith('mailmx') or hostname.startswith('mailrelay'):
                    if client_ip and not self.messages[message_id]["summary"]['client_ip']:
                        self.messages[message_id]["summary"]['client_ip'] = client_ip
                    if client_hostname and not self.messages[message_id]["summary"]['client_hostname']:
                        self.messages[message_id]["summary"]['client_hostname'] = client_hostname

                # We track the hostname for mx, relay and cf servers so we know where the message
                # originated.
                if hostname.startswith('mailmx') or hostname.startswith('mailrelay') or hostname.startswith('mailcf'):
                    # For this field, the earliest field is the most useful. If a message starts with mailcf,
                    # we don't want to overwrite that hostname with mailrelay.
                    if not "hostname" in self.messages[message_id]["summary"]:
                        self.messages[message_id]["summary"]["hostname"] = hostname

                if from_address:
                    self.messages[message_id]["summary"]['from'] = from_address
                    # If we haven't already calculated a sasl username, try shortening
                    # this from address.
                    if not self.messages[message_id]["summary"]['sender']:
                        self.messages[message_id]["summary"]['sender'] = self.fromify(from_address)

                # If we have an orig to address, then the orig to address is
                # the real to address, and the to address is the local delivery
                # or aliased address.
                if orig_to_address:
                    delivery_to_address = orig_to_address
                    delivery_delivered_to_address = to_address
                elif to_address:
                    delivery_to_address = to_address
                
                if relay:
                    if relay.endswith(":25"):
                        # We don't need the port number, shave it off to save space.
                        relay = relay[0:-3]
                    # Assuming we get the logs in the proper order, this should always
                    # be set to the *last* relay host, which is the most useful. But
                    # due to weird timing issues, this might be inaccurate sometimes.
                    delivery_host = relay
                    delivery_status = status 
                    delivery_timestamp = timestamp

                # Now append the delivery dict we may have created to the deliveries
                # list. But only if we have a to_address. If a to address shows up in
                # more then one log line, we overwrite previous ones with data from
                # later ones.
                if delivery_to_address:
                    i = delivery_to_address
                    if i not in delivery_dict.keys():
                        delivery_dict[i] = {}
                    delivery_dict[i]['to_address'] = delivery_to_address
                    if delivery_delivered_to_address:
                        if 'delivered_to_address' not in delivery_dict[i]:
                            delivery_dict[i]['delivered_to_address'] = [delivery_delivered_to_address]
                        elif delivery_delivered_to_address not in delivery_dict[i]['delivered_to_address']:
                            delivery_dict[i]['delivered_to_address'].append(delivery_delivered_to_address)
                    if delivery_host:
                        # If we already have a delivery host, don't overwrite it with a localhost
                        # address.
                        if 'host' in delivery_dict[i]:
                            if not delivery_host.startswith("127"):
                                delivery_dict[i]['host'] = delivery_host 
                        else:
                            delivery_dict[i]['host'] = delivery_host 
                            
                    if delivery_status:
                        delivery_dict[i]['status'] = delivery_status 
                    if delivery_status_message:
                        delivery_dict[i]['status_message'] = delivery_status_message
                    if delivery_timestamp:
                        delivery_dict[i]['timestamp'] = delivery_timestamp 

                if queue_id and queue_id not in self.messages[message_id]["summary"]["queue_ids"]:
                    self.messages[message_id]["summary"]["queue_ids"].append(queue_id)

            self.messages[message_id]["summary"]['deliveries'] = delivery_dict.values()

        end = time.time()
        self.cprint("Python process time: {0}".format(end - start), self.LOG_LEVEL_DEBUG)


    """
    Take a verp style from address that is unique for every recipient and 
    munge it into a predictable short form that is the same for every
    recipient.
    """
    def fromify(self, from_address):
        verp_match = self.verp_re.match(from_address)
        if verp_match:
           return "{0}*{1}".format(verp_match.group(1), verp_match.group(2))
        return ""

