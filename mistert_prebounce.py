#!/usr/bin/env python3

#
# mistert_prebounce: generate a list of undeliverable email addresses
#
# Track bounce and deferred messages matching pre-defined strings and insert
# them into a database so they can be pre-bounced by our mail servers.
#
# Specifically, we look for addresses that bounce two or more times over a
# period of more then 24 hours. This way if an email bounces due to a technical
# problem on the receiving end, we give the mail admins of the receiving mail
# server a solid 24 hours to fix it.
#
# Furthermore, as soon as an email bounces, we check for successful deliveries
# and remove the entry if we find just one successful delivery.
#
# Entries that are more then 6 months old are purged.


import requests
import yaml
import os
import sqlite3
import re
from datetime import datetime
from elastic import Elastic
from pytz import UTC
from mistert_base import MrtBase

class MrtPrebounce(MrtBase):
    # mode should be collect or list.
    mode = 'list' 
    mistert_conf = [ 'mistert.conf', '/etc/mistert.conf' ]
    cur = None
    conn = None
    bounce_patterns = []
    elastic = None

    def __init__(self):
        self.elastic = Elastic()
        # Supress dots. It gets in the way of other output.
        self.elastic.log_level = self.LOG_LEVEL_ERROR
        for conf_path in self.mistert_conf:
            if os.path.exists(conf_path):
                with open(conf_path) as f:
                    conf = yaml.load(f, Loader=yaml.FullLoader)
        if conf is None:
            raise Exception("Failed to find mistert.conf")
        sql_lite_path = conf['sql_lite_path']
        self.bounce_patterns = conf["bounce_patterns"] 

        # Initialize the sqlite database.
        init_db = False
        if not os.path.exists(sql_lite_path):
            init_db = True

        self.conn = sqlite3.connect(conf['sql_lite_path'])
        self.conn.execute('pragma journal_mode=wal')
        self.cur = self.conn.cursor()
        if init_db:
            init_sql = """
               CREATE TABLE IF NOT EXISTS email (
                email TEXT PRIMARY KEY NOT NULL,
                first INT NOT NULL DEFAULT (strftime('%s','now')),
                last INT NOT NULL DEFAULT 0,
                message TEXT
              )
              """
            self.cur.execute(init_sql)
            self.cur.execute("CREATE INDEX firstidx ON email (first)")
            self.cur.execute("CREATE INDEX lastidx ON email (last)")


    """
    Given a list of email addresses that have bounced, check for just one
    successful delivery and, if found, purge the email address from the list
    of bouncing addresses.
    """
    def exonerate(self, bounced_addresses):
        q = {
            "_source": [ 
                "dissect.postfix_to_address" 
            ],
            "sort": [
                "dissect.postfix_to_address",
                "@timestamp"
            ],
            "size": 10000,
            "query": {
                "bool": {
                    "filter": [
                        { "term": { "dissect.postfix_status": "sent"}},
                        { "wildcard": { "host.hostname":  { "value": "mailrelay*"}}},
                    ]
                }
            }
        }

        hits = self.elastic.query_all_terms(q, "dissect.postfix_to_address", bounced_addresses)
        found_emails = []
        for hit in hits:
            email = str(hit["_source"]["dissect"]["postfix_to_address"])
            if email not in found_emails:
                found_emails.append(email)
        count = len(found_emails)
        if count > 0:
            for email in found_emails:
                params = {
                    "email": email
                }
                sql = "DELETE FROM email WHERE email = :email"
                self.cur.execute(sql, params)
                self.cprint("Exonerated email: {0}".format(email), self.LOG_LEVEL_INFO)
        if count > 0:
            self.cprint("Exonerated {0} email addresses.".format(count), self.LOG_LEVEL_INFO)

    def main(self):
        if self.mode == 'list':
            self.list()
        elif self.mode == 'collect':
            self.collect()
        else:
            raise Exception("Unknown mode: {0}".format(self.mode))

    def list(self):
        sql = "SELECT email FROM email WHERE last - first > 86400"
        self.cur.arraysize = 1000
        for result in self.cur.execute(sql):
            print(result[0])

    def collect(self):
        q = {
            "_source": [ 
                "dissect.postfix_to_address", "message", "@timestamp" 
            ],
            "sort": [
                "dissect.postfix_to_address",
                "@timestamp"
            ],
            "size": 10000,
            "query": {
                "bool": {
                    "filter": [
                        { "term": { "systemd.slice": "system-postfix.slice" }},
                        { "term": { "dissect.postfix_status": "bounced"}},
                        { "range": { "@timestamp": { "gt": "now-{0}".format(self.since), "lt": "now-{0}".format(self.until) }}}
                    ],
                    "should": self.bounce_patterns,
                    "minimum_should_match" : 1
                }
            }
        }

        hits = self.elastic.query_all(q)
        update_count = 0
        insert_count = 0
        skipped_count = 0
        self.cprint("Found {0} bounces.".format(len(hits)), self.LOG_LEVEL_INFO)
        email_matcher = re.compile('^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[A-Za-z]{2,4})$')
        for hit in hits:
            time = int(datetime.fromisoformat(hit["_source"]["@timestamp"][:-1]).astimezone(UTC).timestamp())
            email = hit["_source"]["dissect"]["postfix_to_address"]
            if email_matcher.match(email) == None:
                self.cprint("Invalid email {0}".format(email))
                continue
            # Don't match our special email validation address.
            if email.startswith("mayfirst-verify-mx-domain@"):
                self.cprint(f"Not pre-bouncing validation email address {email}.")
            message = hit["_source"]["message"]
            # Select the timestamp from the last time we inerted this record.
            sql = "SELECT last FROM email WHERE lower(email) = lower(:email)"
            params = { 'email': email }
            self.cur.execute(sql, params)
            exists = self.cur.fetchone()
            if exists:
                # We already have this email address. Check if the last timestamp
                # is the same one as the one we are intending to insert.
                if exists[0] == time:
                    # It's the same one we already recorded, skip.
                    # print("We have already recorded {0}.".format(email))
                    skipped_count += 1
                    continue

                # Record exists, update.
                # print("Updating {0}.".format(email))
                params = {
                    "email": email,
                    "last": time,
                    "message": message
                }
                sql = "UPDATE email SET email = :email, last = :last, message = :message WHERE lower(email) = lower(:email)"
                update_count += 1
            else:
                # No email logged, insert with first value.
                params = {
                    "email": email,
                    "first": time,
                    "last": time,
                    "message": message
                }
                #print("Inserting {0}.".format(email))
                sql = "INSERT INTO email (email, last, message) VALUES (:email, :last, :message)"
                insert_count += 1

            #print(params)
            self.cur.execute(sql, params)

        self.cprint("{0} email addresses inserted.".format(insert_count), self.LOG_LEVEL_INFO)
        self.cprint("{0} email addresses updated.".format(update_count), self.LOG_LEVEL_INFO)
        self.cprint("{0} email addresses skipped (already inserted).".format(skipped_count), self.LOG_LEVEL_INFO)

        # Now check for deliveries to previosly bounced emails so we can remove them.

        # Find all bounces where there is less then a 1 day gap between the first and
        # last bounce. In other words: addresses that do not yet qualify for being
        # banned, but have at least one bounce.
        self.cprint("Checking for addresses to exonerate...")
        sql = "SELECT email FROM email WHERE (last - first) < 86400"
        bounced_addresses = []
        for row in self.cur.execute(sql):
            if row[0] not in bounced_addresses:
                bounced_addresses.append(row[0])

        if len(bounced_addresses) > 0:
            self.exonerate(bounced_addresses)

        # Now purge entries older then 6 months.
        self.cprint("Purging...")
        sql = "DELETE FROM email WHERE strftime('%s','now') - (180 * 86400) > last"
        self.cur.execute(sql)
        self.conn.commit()
        self.cprint("{0} entries purged.".format(self.cur.rowcount), self.LOG_LEVEL_INFO)


