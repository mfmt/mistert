#!/usr/bin/env python3

"""
Find bounce messages that indicate our servers are
ina block list.

"""

import requests
import json
import os
import sys
import yaml
import argparse
import re
from elastic import Elastic
from mistert_base import MrtBase

class MrtBlocks(MrtBase):
    # Command line arguments.
    mode = "list" 
    actionable = False
    elastic = None

    def __init__(self):
        self.elastic = Elastic()
        # Supress dots.
        self.elastic.log_level = self.LOG_LEVEL_ERROR

    def main(self):
        self.find_block_list_bounces()

    def find_block_list_bounces(self):
        known_block_strings = [
            'spam: This message was blocked for security reasons',
            'MailFrom domain is listed in Spamhaus',
            'is in a black list at zen.spamhaus.org',
            'DNSBL:RBL',
            'blocked using',
            'ASE reports it as spam',
            'Mensaje rechazado por ser spam',
            'JunkMail rejected',
            'content presents a potential 552-5.7.0 security issue',
            'Message contained unsafe content',
            'The mail server detected your message as spam',
            'Rejected due to high probability of spam',
            'is in a black list at',
            'envelope blocked',
            'block-listed',
            'content judged to be spam',
            'blocked using Cloudmark Sender Intelligence',
            'blocked due to spam content in the message',
            'Rejected by spam filter',
            'likely unsolicited mail',
            'spam or virus classification',
            '550 spam id',
            'spam detected',
            'message is considered spam',
            'on blocklist of user',
            'High probability of spam',
            'rejected due to suspected spam',
            'message was detected as spam',
            'A URL in this email',
            'blocked due to security reason',
            'spam points',
            'appears to be spam',
            'rejected as spam',
            'looks like a spam',
            'blocked using zen.spamhaus.org',
            'spam message rejected',
            'suspected spams',
            'Message contained spam content',
            'suspicious due to the very low reputation of the sending IP',
            'SPAM type=spam',
            'blocked automatically by anti-spam system',
            'protect our users from spam',
        ]
        r_contains_spam_or_block = re.compile('.*(spam|block).*')
        r_contains_known_block_string = re.compile('.*(' + "|".join(known_block_strings) + ').*')
        r_contains_http = re.compile('.*https?://.*')

        q = {
            "size": 10000,
            "sort": [
                { "@timestamp" : {"order": "asc"}},
                { "dissect.postfix_queue_id" : {"order": "asc"}}
            ],
            "_source": [
                    "dissect.postfix_status_message",
                    "dissect.postfix_to_address",
                    "dissect.postfix_queue_id",
                    "dissect.postfix_instance",
                    "dissect.postfix_message_id",
                    "host.hostname"
            ],
            "query": {
                "bool": {
                    "filter": [
                        {"range": { "@timestamp": { "gte": "now-{0}".format(self.since), "lte": "now-{0}".format(self.until) }}},
                        { "term": { "systemd.slice": "system-postfix.slice" }},
                        { "term": { "dissect.postfix_status": "bounced" } }
                    ],
                    "should": [
                        { "wildcard": { "dissect.postfix_instance": { "value": "postfix-bulk*" }}},
                        { "wildcard": { "dissect.postfix_instance": { "value": "postfix-priority*" }}},
                        { "wildcard": { "dissect.postfix_instance": { "value": "postfix-filtered*" }}}
                    ],
                    "minimum_should_match" : 1

               },
            },
        }

        hits = self.elastic.query_all(q)
        count = 0
        blocks = 0
        for hit in hits:
            count += 1
            status_message = hit["_source"]["dissect"]["postfix_status_message"]
            to_address = hit["_source"]["dissect"]["postfix_to_address"]
            instance = hit["_source"]["dissect"]["postfix_instance"]
            hostname = hit["_source"]["host"]["hostname"]
            print_result = False
            if self.mode == "fish":
                if r_contains_spam_or_block.match(status_message) and not r_contains_known_block_string.match(status_message):
                    blocks += 1
                    print_result = True
            else:
                if r_contains_known_block_string.match(status_message):
                    if self.actionable:
                        if r_contains_http.match(status_message):
                            blocks += 1
                            print_result = True
                    else:
                        print_result = True
                        blocks += 1
                        count += 1
            if print_result:
                self.cprint("[c]{}[/c] {} [y]{}[/y] {}".format(to_address, instance, hostname, status_message))

        #sender_map = QueueIdMapper.map_to_senders(queue_id_map)
        #print(senders)
        self.cprint("Bounces: {0}".format(count))
        self.cprint("Blocks: {0}".format(blocks))

if __name__ == "__main__":
  sys.exit(main())


