#!/usr/bin/env python3

import os
import re

"""
Base class to provide common methods.
"""

class MrtBase:
    LOG_LEVEL_ERROR = 'error'
    LOG_LEVEL_INFO = 'info'
    LOG_LEVEL_DEBUG = 'debug'
    log_level = None
    since = "5m"
    until = "0s"

    def __init__(self):
        self.log_level = self.LOG_LEVEL_INFO

    def cprint(self, msg, priority=None, end="\n"):
        if priority is None:
            priority = self.LOG_LEVEL_INFO
        if priority == self.LOG_LEVEL_DEBUG and self.log_level != self.LOG_LEVEL_DEBUG:
            return
        if priority == self.LOG_LEVEL_INFO and self.log_level == self.LOG_LEVEL_ERROR:
            return

        # http://no-color.org/
        if os.environ.get('NO_COLOR'):
            # Just get rid of all the color tags.
            msg = re.sub(r'\[/?(r|g|y|c|p)\]', "", msg)
        else:
            # Replace any closing braces with white.
            msg = re.sub(r'\[/(r|g|y|c|p)\]', "\033[00m", msg)
            # cyan 
            msg = msg.replace('[c]', "\033[96m") 
            # yellow
            msg = msg.replace('[y]', "\033[93m") 
            # green
            msg = msg.replace('[g]', "\033[92m") 
            # red
            msg = msg.replace('[r]', "\033[91m")
            # purple 
            msg = msg.replace('[p]', "\033[94m")

        print(msg, end=end, flush=True)

    def calculate_range(self, since, until):
        seconds_until = 0
        seconds_since = 0
        if since.endswith('s'):
            seconds_since = int(since[0:-1])
        elif since.endswith('m'):
            seconds_since = 60 * int(since[0:-1])
        elif since.endswith('h'):
            seconds_since = 60 * 60 * int(since[0:-1])
        elif since.endswith('d'):
            seconds_since = 60 * 60 * 24 * int(since[0:-1])
        elif since.endswith('w'):
            seconds_since = 60 * 60 * 24 * 7 * int(since[0:-1])
        if until.endswith('s'):
            seconds_until = int(until[0:-1])
        elif until.endswith('m'):
            seconds_until = 60 * int(until[0:-1])
        elif until.endswith('h'):
            seconds_until = 60 * 60 * int(until[0:-1])
        elif until.endswith('d'):
            seconds_until = 60 * 60 * 24 * int(until[0:-1])
        elif until.endswith('w'):
            seconds_until = 60 * 60 * 24 * 7 * int(until[0:-1])
        return seconds_since - seconds_until
